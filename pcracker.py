#!/usr/bin/python

import crypt

shadowDict = {}
hashList = []

def loadShadow():
    shadowInput = raw_input("Enter Shadow File Name:")
    try:
        shadowFile = open(shadowInput,'r')
        for line in shadowFile.readlines():
            if ":" in line:
                user = line.split(':')[0]
                cryptWord =  line.split(':')[1].strip(' ')
                shadowDict[user] = cryptWord
        return
    except IOError:
        print "Could not read file:", shadowFile
    
def loadHashList():
    hashListInput = raw_input("Enter Dictionary File Name:")
    
    with open(hashListInput) as file:
        for line in file:
            line = line.strip('\n') #preprocess line
            hashList.append(line)
    

def crackPassword(shadowDict,hashList):
    for key, value in shadowDict.iteritems():
        salt = value[0:19]
        for word in hashList:
            cryptedPass = crypt.crypt(word,salt)
            if (value == cryptedPass):
                print 'User Name : ' ,key , ' Password : ', word

    
def main():
    loadShadow()
    loadHashList()
    crackPassword(shadowDict,hashList)
        
main()